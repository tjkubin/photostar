module.exports = {
  parser: 'babel-ts',
  trailingComma: 'all',
  singleQuote: true,
  printWidth: 120,
};
