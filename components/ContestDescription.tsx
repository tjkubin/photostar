import useToggle from '../hooks/useToggle';
import classNames from 'classnames';

export default function ContestDescription() {
  return (
    <>
      <div className="flex flex-col w-full mx-auto mb-12 text-left lg:w-2/3 lg:text-center">
        <h2 className="mb-1 text-xs font-semibold tracking-widest text-blue-600 uppercase title-font">
          Obec Zákolany pořádá
        </h2>
        <h1 className="mb-6 text-4xl font-semibold tracking-tighter text-black sm:text-6xl title-font dark:text-white">
          Fotosoutěž
        </h1>
        <p className="pb-10 mx-auto text-base font-light leading-relaxed tracking-wider text-gray-700 lg:w-4/5 dark:text-gray-300">
          Vážení přátelé, sousedé, kamarádi! Před pár lety se zrodil nápad zorganizovat{' '}
          <span className="text-blue-600">amatérskou fotografickou soutěž</span>. A teď, v době covidové a v době iPhonů
          se nám skvěle nabízí tuto soutěž zveřejnit a s pomocí nás všech i zrealizovat! Je to přesně doba, kdy díky
          všem možným striktním proticovidovým opatřením máme alespoň příležitost, kterou většina z nás ráda využívá,
          chodit na kratší i delší procházky a pomocí mobilu v kapse zvěčňovat prožité okamžiky, které zažíváme sami
          nebo se svými blízkými. A tak díky technickým vymoženostem dnešní doby, kdy se fotografování díky chytrým
          telefonům stalo zábavou, koníčkem, vášní a někdy závislostí se z řady z nás stali vášniví amatérští
          fotografové. Fotografové kteří mají potřebu zachytit a zvěčnit plynoucí čas a život kolem nás.
        </p>
        <h2 className="mb-1 text-xs font-semibold tracking-widest text-blue-600 uppercase title-font">
          Veřejné hlasování
        </h2>
        <h1 className="mb-6 text-3xl font-semibold tracking-tighter text-black sm:text-4xl title-font dark:text-white">
          O nejhezčí fotografie
        </h1>
        <p className="mx-auto text-base font-light leading-relaxed tracking-wider text-gray-700 lg:w-4/5 dark:text-gray-300">
          Vážení přátelé, koncem května jsme ukončili příjem fotografií, které byly nominované do amatérské fotosoutěže
          <span className="text-blue-600">Budečsko v obrazech</span>. Celkem bylo do soutěže přihlášeno 83 snímků.
          Všechny nominované fotografie byly již předány k posouzení odborné porotě složené z Otty M. Urbana, Filipa
          Wittlicha a Marka Višňovského. Nyní nastal čas, kdy se může k hlasování o nejhezčí fotografie přidat i široká
          veřejnost. Pravidla pro hlasování: hlasování se provádí pomocí hvězdiček, které jsou umístěny pod každou
          fotografií. Ohodnotit můžete každou fotografii zvlášť. Pokud soutěžící uvedli název fotografie – je napsán pod
          fotografií, někteří soutěžící bohužel název neuvedli – fotografie je tedy bez pojmenování. U fotografií nejsou
          zveřejněni autoři aby nedocházelo k případnému ovlivňování – hlasování je tedy zcela anonymní – a to jak pro
          veřejnou, tak i pro odbornou porotu. Hlasování bude ukončeno 31. 8. 2021. Každý může hlasovat z jednoho
          zařízení a prohlížeče. Hlasování se mohou zúčastnit i soutěžící. V měsíci září budou prostřednictvím webových
          stránek obce Zákolany zveřejněny vítězné fotografie. (Případné dotazy na tel.č. 602 118 564 – Vráťa K.)
        </p>
      </div>
      {/* <ContestRules /> */}
      <div className="flex flex-col w-full px-0 mx-auto lg:w-2/3 sm:flex-row md:px-8" style={{ display: 'none' }}>
        <input
          className="flex-grow w-full px-4 py-2 mb-4 mr-4 text-base text-black transition duration-1000 ease-in-out transform rounded-lg bg-blueGray-200 focus:outline-none focus:border-purple-500 sm:mb-0 focus:bg-white focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2"
          placeholder="Vaše jméno"
          type="text"
        />
        <input
          className="flex-grow w-full px-4 py-2 mb-4 mr-4 text-base text-black transition duration-1000 ease-in-out transform rounded-lg bg-blueGray-200 focus:outline-none focus:border-purple-500 sm:mb-0 focus:bg-white focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2"
          placeholder="Váš e-mail"
          type="email"
        />
        <button className="w-1/2 px-8 py-2 font-semibold text-white transition duration-500 ease-in-out transform bg-blue-800 rounded-lg hover:bg-blue-600 focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2">
          Přihlášení
        </button>
      </div>
    </>
  );
}

function ContestRules() {
  const [isOn, toggle] = useToggle();
  const style = { ...(isOn ? {} : { display: 'none' }) };

  return (
    <div className="flex flex-col w-full mx-auto my-8 text-left border-4 border-gray-800 rounded-lg hover:bg-gray-800 easy-transition lg:w-2/3 lg:text-center">
      <button onClick={() => toggle()} className="w-full px-10 py-2 focus:shadow-outline focus:outline-none ">
        <div className="flex flex-row items-center justify-center w-full text-xl font-semibold tracking-widest text-blue-600">
          Jak se soutěže zúčastnit?
          <div
            className={classNames(
              'flex',
              'flex-row',
              'items-center',
              'justify-center',
              'w-20',
              'h-20',
              'text-blue-600',
              'origin-center',
              'transform',
              'transition-transform',
              ' duration-200',
              'ease-in-out',
              { 'rotate-90': isOn },
            )}
          >
            <svg width="5rem" height="5rem" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
              <path
                fillRule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clipRule="evenodd"
              />
            </svg>
          </div>
        </div>
      </button>
      <div
        className="flex flex-col p-8 font-light leading-relaxed tracking-wider text-left lg:text-center"
        style={style}
      >
        <p>Jak již téma soutěže napovídá, fotografovat lze vše v okolí Budče, Zákolan, Trněného Újezdu a Kovár.</p>
        Každý, kdo se chce soutěže zúčastnit, nominuje 1, maximálně 2 své nejlepší fotografie za jedno dané téma. Téma
        si můžete vybrat jedno, nebo všechny tři – záleží na Vás. Můžete tedy poslat 2 fotografie za 1 téma nebo
        maximálně 6 fotografií za všechny 3 témata. Své nominované fotografie zašlete v elektronické podobě na adresu
        <p className="py-4 text-blue-600 text-bold">fotosoutez.zakolany@protonmail.com</p>
        do
        <p className="py-4 text-blue-600 text-bold">10. 6. 2021</p>
        Ke každé fotografii přidejte stručný popis. Například její název, případně odkud je nebo co Vás napadlo při
        focení daného okamžiku. Věkové kategorie jsou dvě: děti do 11 let a náctiletí společně s dospělými 12+. Na
        fotografování si dáme trochu více času, abychom mohli zachytit vícero ročních období (zimu, jaro i léto). Po
        uzávěrce bude v elektronické podobě možnost veřejného hlasování o nejhezčí foto z každého tématu a samozřejmě
        i hlasování odborné poroty, která vybere fotografie dle uměleckého hlediska. Nejúspěšnější fotografie bychom
        rádi využili pro nové pohlednice, webové stránky obce, nástěnný kalendář, pro kronikovou dokumentaci. Pokud
        pandemická situace na podzim dovolí, mohli bychom zorganizovat i výstavu. Tak tedy „pojďme a foťme“!
      </div>
    </div>
  );
}
