import { Contest, contests } from '../lib/constants';
import ContestItem from './ContestItem';

export default function ContestGrid() {
  return (
    <div className="grid grid-cols-1 gap-6 lg:grid-cols-2 xl:grid-cols-3">
      {contests.map((contest: Contest) => (
        <ContestItem contest={contest} />
      ))}
    </div>
  );
}
