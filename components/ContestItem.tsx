import { useState } from 'react';
import Link from 'next/link';
import classNames from 'classnames';

import { Contest } from '../lib/constants';

export default function ContestItem(props: { contest: Contest }) {
  const [isHover, setHover] = useState<boolean>(false);
  const { contest } = props;

  const setHoverOn = () => setHover(true);
  const setHoverOff = () => setHover(false);

  return (
    <Link
      key={contest.key}
      href={{
        pathname: '/contest/[slug]',
        query: { slug: contest.slug },
      }}
    >
      <a
        className="h-64 my-12 bg-white bg-opacity-0 bg-center bg-cover rounded-lg easy-transition group"
        style={{ backgroundImage: `url(${contest.image})` }}
        onMouseEnter={setHoverOn}
        onMouseLeave={setHoverOff}
      >
        <div className="flex flex-col items-center justify-center h-full rounded-lg hover:bg-opacity-60 hover:bg-black easy-transition">
          <div className="text-3xl text-center text-gray-200 align-middle md:text-4xl ld:text-5xl group-hover:text-white text-bold">
            {contest.title}
          </div>
          <div
            className={classNames('mt-4 text-xl text-center text-gray-200 align-middle text-bold easy-transition', {
              hidden: !isHover,
            })}
          >
            <ul>
              <li>
                <b> {contest.count} fotografií </b>
              </li>
            </ul>
          </div>
        </div>
      </a>
    </Link>
  );
}
