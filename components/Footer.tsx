export default function Footer() {
  return (
    <div className="flex-col mt-12 mb-8">
      <p className="w-full text-xs text-center font-extralight">
        Tato webová stránka používá cookies. Užíváním těchto webových stránek vyjadřujete souhlas s jejich používáním.
      </p>
      <p className="w-full mt-2 text-sm text-center text-gray-500 ">
        <a
          href="http://www.zakolany.cz"
          className="inline-flex items-center font-semibold text-blue-600 md:mb-2 lg:mb-0 hover:text-blue-400 "
        >
          Obec Zákolany
          <svg
            className="w-4 h-4 ml-2"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            width="20"
            height="20"
            fill="currentColor"
          >
            <path fill="none" d="M0 0h24v24H0z" />
            <path d="M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z" />
          </svg>
        </a>
      </p>
    </div>
  );
}
