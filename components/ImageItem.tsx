import { useEffect, useState } from 'react';
import { ImageInfo } from '../pages/contest/[slug]';
import StarLine from './StarLine';

const PHOTO_DIR_WEB = '/photos';

const voteCall = async (slug, index, stars) => {
  const resp = await fetch(`/api/vote?slug=${slug}&index=${index}&stars=${stars}`);
  const data = await resp.json();

  return data;
};

const ImagePlaceholder = () => <div style={{ width: '100%', height: '50vw', background: '' }}></div>;

export default function ImageItem({
  image,
  stars = 0,
  isVotingEnabled = false,
}: {
  image: ImageInfo;
  stars: number;
  isVotingEnabled: boolean;
}) {
  const imageSrc = `${PHOTO_DIR_WEB}/${image.file}`;

  const [isImageLoading, setIsImageLoading] = useState(true);

  useEffect(() => {
    const imageToLoad = new Image();
    imageToLoad.src = imageSrc;
    imageToLoad.onload = () => {
      setIsImageLoading(false);
    };
  }, []);

  const onVote = async (stars: number) => {
    // console.log(`voting for ${image.index}: ${stars} stars`);
    const data = await voteCall(image.slug, image.index, stars);
  };

  return (
    <div
      key={image.index}
      className="flex flex-col items-center justify-center h-full p-10 rounded-lg hover:bg-opacity-60 hover:bg-black easy-transition"
    >
      {isImageLoading ? (
        <ImagePlaceholder />
      ) : (
        <>
          <img src={imageSrc} />
          <div
            key="title"
            className="mt-5 text-2xl text-center text-gray-200 align-middle sm:text-lg md:text-4xl ld:text-5xl group-hover:text-white text-bold"
          >
            {image.title}
          </div>
          {isVotingEnabled && (
            <div key="stars" className="flex mt-4 mb-5 align-middle">
              <StarLine vote={onVote} initStars={stars} />
            </div>
          )}
        </>
      )}
    </div>
  );
}
