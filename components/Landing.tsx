import { v4 as uuid } from 'uuid';

import Layout from '../components/Layout';
import ContestGrid from '../components/ContestGrid';
import ContestDescription from '../components/ContestDescription';
import Footer from '../components/Footer';
import Cookies from '../lib/cookies';
import { CONSENT_COOKIE_NAME } from '../lib/constants';

if (!Cookies.get(CONSENT_COOKIE_NAME)) {
  const uuidString = uuid();
  // console.log('Going to set cookie: ', uuidString);
  Cookies.set(CONSENT_COOKIE_NAME, uuidString);
}

export default function LandingPage() {
  return (
    <Layout>
      <ContestDescription />
      <ContestGrid />
      <Footer />
    </Layout>
  );
}
