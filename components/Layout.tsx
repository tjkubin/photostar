export default function layout({ children }: { children: React.ReactNode }) {
  return <div className="container px-8 mx-auto pt-36 lg:px-4">{children}</div>;
}
