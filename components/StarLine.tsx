import React, { useState } from 'react';
import StarIconFilled from './Icons/StarIconFilled';
import StarIconOutline from './Icons/StarIconOutline';
import styles from './StarLine.module.css';

type VoteCallback = (stars: number) => void;

const range = (length: number): number[] => Array.from(Array(length).keys());

const NUMBER_OF_STARS = 5;
const STAR_VECTOR = range(NUMBER_OF_STARS);
const iconSizeRem = 1;
const paddingRem = 0.3;
const wrapperHeightRem = iconSizeRem + 2 * paddingRem;
const wrapperWidthRem = NUMBER_OF_STARS * (iconSizeRem + 2 * paddingRem);
const iconSize = `${iconSizeRem}rem`;

const StarLine = ({ vote, initStars }: { vote: VoteCallback; initStars: number }) => {
  const [starCount, setStarCount] = useState<number>(initStars);

  const handleOnClick = (i: number) => () => {
    const stars = NUMBER_OF_STARS - i;
    setStarCount(stars);
    vote(stars);
  };

  return (
    <div className={styles.starWrapper} style={{ width: `${wrapperWidthRem}rem`, height: `${wrapperHeightRem}rem` }}>
      <div className={styles.visibleStarBox}>
        {STAR_VECTOR.map((i) => (
          <div key={i} className={i < starCount ? styles.starActive : styles.star}>
            <StarIconFilled width={iconSize} height={iconSize} />
          </div>
        ))}
      </div>

      {STAR_VECTOR.map((i) => {
        const STAR_IN_BOX = range(NUMBER_OF_STARS - i);
        return (
          <div key={i} onClick={handleOnClick(i)} className={styles.starBox}>
            {STAR_IN_BOX.map((j) => (
              <div key={j} className={styles.starOutline}>
                <StarIconOutline width={iconSize} height={iconSize} />
              </div>
            ))}
          </div>
        );
      })}
    </div>
  );
};

export default StarLine;
