export const CONSENT_COOKIE_NAME = 'AB_AC_CD_PHOTO_STAR_CONSENT';
export const CONSENT_COLLECTION_NAME = 'contest2021';

export type Contest = {
  title: string;
  image: string;
  key: number;
  slug: string;
  count: number;
};

export enum SLUG {
  NATURE = 'nature',
  PEOPLE = 'people',
  ARCHITECTURE = 'architecture',
}

export const slugMap = {
  1: SLUG.NATURE,
  2: SLUG.PEOPLE,
  3: SLUG.ARCHITECTURE,
};

export const contests: Contest[] = [
  {
    key: 2,
    slug: 'people',
    title: 'Lidé',
    image: 'img/people.jpg',
    count: 19,
  },
  {
    key: 1,
    slug: 'nature',
    title: 'Příroda a krajina',
    image: 'img/tree3.jpg',
    count: 31,
  },
  {
    key: 3,
    slug: 'architecture',
    title: 'Architektura',
    image: 'img/architecture.jpg',
    count: 33,
  },
];
