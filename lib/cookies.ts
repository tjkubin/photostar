// const cookieDomain = 'photostar.vercel.app' 'tjkubin.vercel.app'

class Cookies {
  static set(name: string, value: string): boolean {
    const expirationDate = new Date(Date.now() + 365 * 24 * 60 * 60 * 1000); // 1 year
    // const cookie = `${name}=${value}; expires=${expirationDate.toUTCString()}; path=/; domain=${cookieDomain}`;
    const cookie = `${name}=${value}; expires=${expirationDate.toUTCString()}`;
    document.cookie = cookie;
    return document.cookie.indexOf(`${name}=`) === -1;
  }

  static remove(name: string): boolean {
    // const cookie = `${name}=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/; domain=${cookieDomain}`;
    const cookie = `${name}=; expires=Thu, 01 Jan 1970 00:00:01 GMT`;
    document.cookie = cookie;
    return document.cookie.indexOf(`${name}=`) === -1;
  }

  static get(name: string): string | null {
    const cookies = document.cookie.split('; ');
    const cookie = cookies.find((c) => c.startsWith(`${name}=`));
    if (!cookie) {
      return null;
    }
    return cookie.substr(cookie.indexOf('=') + 1);
  }

  static removeAll() {
    const cookies = document.cookie.split('; ');
    cookies.forEach((cookie) => Cookies.remove(cookie.substr(0, cookie.indexOf('='))));
  }
}

export default Cookies;
