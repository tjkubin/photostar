import { connectToDatabase } from './mongodb';

const connection = connectToDatabase();

export default connection;
