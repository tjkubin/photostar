import { connectToDatabase } from '../../lib/mongodb';
import { CONSENT_COLLECTION_NAME } from '../../lib/constants';

export default async (req, res) => {
  const slug = req.query.slug;

  const { client, db } = await connectToDatabase();
  const isConnected = await client.isConnected();
  const collection = db.collection(CONSENT_COLLECTION_NAME);

  const aggregation = [
    {
      $group: {
        _id: '$userId',
      },
    },
    {
      $group: {
        _id: null,
        participants: {
          $sum: 1,
        },
      },
    },
  ];
  const resp = await collection.aggregate(aggregation).toArray();

  res.status(200).json({ isConnected, participants: resp?.[0]?.participants });
};
