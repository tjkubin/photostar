import { connectToDatabase } from '../../lib/mongodb';
import parseCookies from '../../lib/parseCookies';
import { CONSENT_COLLECTION_NAME, CONSENT_COOKIE_NAME } from '../../lib/constants';

export default async (req, res) => {
  const slug = req.query.slug;

  const { client, db } = await connectToDatabase();
  const isConnected = await client.isConnected();
  const collection = db.collection(CONSENT_COLLECTION_NAME);

  const aggregation = [
    { $match: { slug } },
    {
      $group: {
        _id: '$index',
        starsCount: {
          $sum: {
            $toInt: '$stars',
          },
        },
      },
    },
    {
      $sort: {
        starsCount: -1,
      },
    },
    // { rename: { _id: 'index' } },
  ];
  const stars = await collection.aggregate(aggregation).toArray();

  res.status(200).json({ isConnected, stars });
};
