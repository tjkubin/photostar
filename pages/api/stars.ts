// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { connectToDatabase } from '../../lib/mongodb';
import parseCookies from '../../lib/parseCookies';
import { CONSENT_COLLECTION_NAME, CONSENT_COOKIE_NAME } from '../../lib/constants';

const { VOTING } = process.env;

export default async (req, res) => {
  if (VOTING !== 'enabled') {
    return res.status(404).json({});
  }
  const cookies = parseCookies(req);
  const userId = cookies[CONSENT_COOKIE_NAME];

  const slug = req.query.slug;

  const { client, db } = await connectToDatabase();
  const isConnected = await client.isConnected();
  const collection = db.collection(CONSENT_COLLECTION_NAME);

  const aggregation = [{ $match: { userId, slug } }, { $unset: ['_id', 'userId', 'slug'] }];
  const stars = await collection.aggregate(aggregation).toArray();

  res.status(200).json({ isConnected, stars, userId });
};
