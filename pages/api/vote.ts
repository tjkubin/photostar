// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { connectToDatabase } from '../../lib/mongodb';
import parseCookies from '../../lib/parseCookies';
import { CONSENT_COLLECTION_NAME, CONSENT_COOKIE_NAME, contests } from '../../lib/constants';

// export const getPhotoSlugAndIndex = (photoId: string) => {
//   const [slugKey, index] = photoId.split('-');
//   const slugKeyParsed = parseInt(slugKey, 10);
//   const contestDef = contests.find(({ key }) => key === slugKeyParsed);
//   if (contestDef === undefined || index === undefined) {
//     return {};
//   }
//   const { slug } = contestDef;
//   return { slug, index };
// };

const { VOTING } = process.env;

export default async (req, res) => {
  if (VOTING !== 'enabled') {
    return res.status(404).json({});
  }

  const cookies = parseCookies(req);
  const userId = cookies[CONSENT_COOKIE_NAME];

  const slug = req.query.slug;
  const index = req.query.index;
  const stars = req.query.stars;

  const { db } = await connectToDatabase();
  const collection = db.collection(CONSENT_COLLECTION_NAME);

  if (!slug || !index) {
    return res.status(500).json('slug or photo index not set');
  }
  const query = { userId, slug, index };
  const update = { $set: { userId, slug, index, stars } };

  const resp = await collection.updateOne(query, update, { upsert: true });

  res.status(200).json({ success: true });
};
