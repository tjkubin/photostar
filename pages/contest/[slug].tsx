import { useState } from 'react';
import { useRouter } from 'next/router';
import { promises as fs } from 'fs';
import path from 'path';
import useSwr from 'swr';

import ImageItem from '../../components/ImageItem';
import Layout from '../../components/Layout';
import { contests, SLUG } from '../../lib/constants';
import images from '../../lib/contestImagesConfig';

const fetcher = (url) => fetch(url).then((res) => res.json());

export type ImageInfo = {
  file: string;
  title: string;
  slug: string;
  index: number;
};

type Images = {
  [slug: string]: ImageInfo[];
};

export default function Contest(props: { images: Images }) {
  const router = useRouter();
  const slug = router.query.slug as string;
  const { data, error } = useSwr(`/api/stars?slug=${slug}`, fetcher);
  const [isVotingEnabled, setIsVotingEnabled] = useState(false);

  if (!error && data && data.isConnected && data.stars && !isVotingEnabled) {
    setIsVotingEnabled(true);
  }

  const selectedImages = images[slug];
  const contest = contests.find((item) => item.slug === slug);
  const stars =
    data && data.stars
      ? data.stars.reduce((acc, val) => {
          acc[val.index] = parseInt(val.stars, 10);
          return acc;
        }, {})
      : {};

  return (
    <Layout>
      <div className="flex flex-col w-full mx-auto mb-12 text-left lg:w-2/3 lg:text-center">
        <h2 className="mb-1 text-xs font-semibold tracking-widest text-blue-600 uppercase title-font">Kategorie</h2>
        <h1 className="mb-6 text-4xl font-semibold text-black sm:text-6xl title-font dark:text-white">
          {contest.title}
        </h1>
        <div className="mt-5">
          {selectedImages.map((image) => (
            <ImageItem image={image} stars={stars[image.index]} isVotingEnabled={isVotingEnabled} />
          ))}
        </div>
      </div>
    </Layout>
  );
}

// const parseImageFileName = (name) => {
//   const fields = name.split('.');
//   const parts = fields[0].split('-');
//   return {
//     slug: slugMap[parts[0]],
//     title: parts[2],
//     // id: `${parts[0]}-${parts[1]}`,
//     index: parseInt(parts[1], 10),
//   };
// };

// const readPhotos = async () => {
//   const defaults = { people: [], nature: [], architecture: [] } as any;
//   const PHOTOS_DIR = path.join(process.cwd(), 'public/photos');

//   try {
//     const images = await fs.readdir(PHOTOS_DIR);

//     return images.reduce((acc, val) => {
//       const { slug, title, index } = parseImageFileName(val);

//       if (!slug) {
//         return acc;
//       }

//       acc[slug].push({
//         file: val,
//         title,
//         slug,
//         index,
//       });

//       return acc;
//     }, defaults);
//   } catch (err) {
//     console.log('photos dir: ', PHOTOS_DIR);
//     console.log('error reading directory: ', err);
//     return defaults;
//   }
// };

export async function getStaticProps() {
  return {
    props: {
      images,
    },
  };
}

export async function getStaticPaths() {
  return {
    paths: [
      { params: { slug: SLUG.NATURE } },
      { params: { slug: SLUG.PEOPLE } },
      { params: { slug: SLUG.ARCHITECTURE } },
    ],
    fallback: false,
  };
}
