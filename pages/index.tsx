import dynamic from 'next/dynamic';

const LandingPageWithNoSSR = dynamic(() => import('../components/Landing'), { ssr: false });

export default function Home() {
  return <LandingPageWithNoSSR />;
}

// export const getStaticProps: GetStaticProps = async () => {
//   const allPostsData = getSortedPostsData();
//   return {
//     props: {
//       allPostsData,
//     },
//   };
// };
