import useSwr from 'swr';

const fetcher = (url) => fetch(url).then((res) => res.json());

const Results = ({ slug }: { slug: string }) => {
  const { data, error } = useSwr(`/api/results?slug=${slug}`, fetcher);

  if (!data) return null;
  if (error) return null;

  console.log('data: ', data);

  return (
    <>
      <div className="flex items-center justify-center bg-gray-900">
        <div className="overflow-auto lg:overflow-visible">
          <h2 className="justify-center p-5 text-3xl font-semibold tracking-tighter text-center text-blue-600 capitalize">
            {slug}
          </h2>
          <table className="table space-y-6 text-sm text-gray-400 border-separate ">
            <tbody>
              <tr className="bg-gray-800">
                <th className="p-3 text-gray-500">index</th>

                {data.stars.map((element: { _id: string; starsCount: number }) => (
                  <td className="p-3">{element._id}</td>
                ))}
              </tr>
              <tr className="bg-gray-800">
                <th className="p-3 text-gray-500">stars</th>
                {data.stars.map((element: { _id: string; starsCount: number }) => (
                  <td className="p-3">{element.starsCount}</td>
                ))}
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
};

const Participants = () => {
  const { data, error } = useSwr(`/api/participants`, fetcher);

  if (!data) return null;
  if (error) return null;

  console.log('data: ', data);
  return (
    <div className="flex items-center justify-center bg-gray-900">
      <div className="overflow-auto lg:overflow-visible">
        <h2 className="justify-center p-5 text-3xl font-semibold tracking-tighter text-center text-blue-600 capitalize">
          Participants: {data.participants}
        </h2>
      </div>
    </div>
  );
};

export default function ResultsPage() {
  return (
    <>
      <Results slug="people" />
      <Results slug="nature" />
      <Results slug="architecture" />
      <Participants />
    </>
  );
}
