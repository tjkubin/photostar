module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class', // false or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: (theme) => ({
        'contest-one': "url('/img/stavba.jpeg')",
      }),
    },
    scale: {
      '101': '1.01',
      '102': '1.02',
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
