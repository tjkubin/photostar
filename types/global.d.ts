import { Db, MongoClient } from 'mongodb';
import { Mongo } from '../util/mongodb';

namespace NodeJS {
  interface Global {
    mongo: Mongo;
  }
}
